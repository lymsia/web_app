# PreInstallment

- Docker

# Concept

fullstack Django with database (2 containers)

# How to run locally

1. go to root folder (docker-compose.yml should exists)
2. run "docker-compose up -d" (to run all containers on background)
3. run "docker exec -it web_1 bash" to get inside container
4. run "./manage.py migrate" to prep db model
5. run "./manage.py import_news *api_key*" to get 100 latest news from newsapi.org
6. access url http://localhost:8000/news
7. run "docker-compose down" (to shutdown all containers)
