from django.db import models
from django_mysql.models import JSONField


class News(models.Model):

    id = models.AutoField(primary_key=True)
    url = models.TextField()
    source = JSONField()
    author = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    urlToImage = models.TextField(blank=True, null=True)
    publishedAt = models.DateTimeField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'news'

    def __str__(self):
        return '{}'.format(self.name)
