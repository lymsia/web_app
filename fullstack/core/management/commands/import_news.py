from django.core.management.base import BaseCommand, CommandError
from newsapi import NewsApiClient
from core.models import News


class Command(BaseCommand):
    help = 'Import latest 100 news from newsapi.org'

    def add_arguments(self, parser):
        parser.add_argument('api_key')

    def handle(self, *args, **kwargs):
        api_key = kwargs.get('api_key', None)
        if not api_key:
            raise CommandError('need api_key to run')

        newsapi = NewsApiClient(api_key=api_key)
        # import latest 100 news data
        for i in range(5):
            # get 20 news each
            res = newsapi.get_everything(q='web', language='en', page=i+1, sort_by='publishedAt')
            for article in res['articles']:
                # validate if articles already exists
                isExists = News.objects.filter(url=article['url']).first()
                if isExists:
                    continue

                # add new articles to db
                News.objects.create(
                    url=article['url'],
                    source=article['source'],
                    author=article['author'],
                    title=article['title'],
                    description=article['description'],
                    urlToImage=article['urlToImage'],
                    publishedAt=article['publishedAt'],
                    content=article['content']
                )

            print('imported page ' + i + 'out of 5')

        print('all news data have been imported')
