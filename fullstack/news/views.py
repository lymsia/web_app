from django.views.generic import ListView
from core.models import News


class NewsListView(ListView):
    model = News
    paginate_by = 20
    ordering_by = 'publishedAt'
    queryset = News.objects.all()
